﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hostel
{
    public partial class EditForm : Form
    {
        private readonly int id;

        readonly bool edit;
        public EditForm()
        {
            InitializeComponent();
            roomsTableAdapter.Fill(hostelDataSet.Rooms);
            benefitsTableAdapter.Fill(hostelDataSet.Benefits);
            edit = false;
        }

        public EditForm(int id, String name, string gender, string address, string group, string passport, int benefitCode, int roomNumber, DateTime colonizeDate) : this()
        {
            roomsTableAdapter.Fill(hostelDataSet.Rooms);
            benefitsTableAdapter.Fill(hostelDataSet.Benefits);
            edit = true;
            this.id = id;
            textBox_name.Text = name;
            dateTimePicker_ColonizeDate.Value = colonizeDate;
            switch (gender.ToUpper())
            {
                case "мужской":
                    comboBox_Gender.SelectedIndex = 0;
                    break;
                case "женский":
                    comboBox_Gender.SelectedIndex = 1;
                    break;
                default:
                    comboBox_Gender.SelectedIndex = 0;
                    break;
            }
            textBox_Adress.Text = address;
            textBox_Group.Text = group;
            comboBox_Benefit.SelectedValue = benefitCode;
            textBox_Passport.Text = passport;
            comboBox_Room.SelectedValue = roomNumber;


        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'hostelDataSet.Rooms' table. You can move, or remove it, as needed.
            this.roomsTableAdapter.Fill(this.hostelDataSet.Rooms);
            // TODO: This line of code loads data into the 'hostelDataSet.Benefits' table. You can move, or remove it, as needed.
            this.benefitsTableAdapter.Fill(this.hostelDataSet.Benefits);

        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            string gender = "";
            if (comboBox_Gender.SelectedIndex == 0)
                gender = "мужской";
            else if (comboBox_Gender.SelectedIndex == 1)
                gender = "женский";
            if (edit)
            {
                studentsTableAdapter.UpdateQuery(textBox_name.Text, gender, textBox_Adress.Text, textBox_Group.Text, textBox_Passport.Text, Convert.ToInt32(comboBox_Benefit.SelectedValue),
Convert.ToInt32(comboBox_Room.SelectedValue), dateTimePicker_ColonizeDate.Value.ToString(), id);
                studentsTableAdapter.Update(this.hostelDataSet.Students);

            }
            else
            {
                Random random = new Random();
                int index = random.Next();
                studentsTableAdapter.Insert(index, textBox_name.Text, gender, textBox_Adress.Text, textBox_Group.Text, textBox_Passport.Text, Convert.ToInt32(comboBox_Benefit.SelectedValue), Convert.ToInt32(comboBox_Room.SelectedValue), dateTimePicker_ColonizeDate.Value);
                studentsTableAdapter.Update(this.hostelDataSet.Students);
            }
            Close();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
