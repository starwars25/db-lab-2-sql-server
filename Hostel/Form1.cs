﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hostel
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'hostelDataSet.Students' table. You can move, or remove it, as needed.
            this.studentsTableAdapter.Fill(this.hostelDataSet.Students);
            // TODO: This line of code loads data into the 'hostelDataSet.Rooms' table. You can move, or remove it, as needed.
            this.roomsTableAdapter.Fill(this.hostelDataSet.Rooms);
            // TODO: This line of code loads data into the 'hostelDataSet.Benefits' table. You can move, or remove it, as needed.
            this.benefitsTableAdapter.Fill(this.hostelDataSet.Benefits);

            dataGridView1.AutoGenerateColumns = true;
            // TODO: This line of code loads data into the 'labDataSet.Льготы' table. You can move, or remove it, as needed.
            //this.льготыTableAdapter.Fill(this.labDataSet.Льготы);
            // TODO: This line of code loads data into the 'labDataSet.Студенты' table. You can move, or remove it, as needed.
            //this.студентыTableAdapter.Fill(this.labDataSet.Студенты);
            // TODO: This line of code loads data into the 'labDataSet.Комнаты' table. You can move, or remove it, as needed.
            //this.комнатыTableAdapter.Fill(this.labDataSet.Комнаты);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            roomsTableAdapter.Update(hostelDataSet.Rooms);
            benefitsTableAdapter.Update(hostelDataSet.Benefits);
            studentsTableAdapter.Update(hostelDataSet.Students);
        }

        private void roomsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bindingNavigator1.BindingSource = roomsBindingSource;
            dataGridView1.DataSource = roomsBindingSource;
            label1.Text = "Rooms";
        }

        private void benefitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bindingNavigator1.BindingSource = benefitsBindingSource;
            dataGridView1.DataSource = benefitsBindingSource;
            label1.Text = "Benefits";

        }

        private void studentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bindingNavigator1.BindingSource = studentsBindingSource;
            dataGridView1.DataSource = studentsBindingSource;
            label1.Text = "Students";

        }

        private void resettlementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rs = new RSForm();
            rs.ShowDialog();
            benefitsTableAdapter.Fill(hostelDataSet.Benefits);
            studentsTableAdapter.Fill(hostelDataSet.Students);
            roomsTableAdapter.Fill(hostelDataSet.Rooms);
        }

        private void queryEditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var qe = new QueryEdit();
            qe.Show();
        }

        private bool edit = true;


        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!edit) return;
            var edt = new EditForm();
            edt.ShowDialog();
            studentsTableAdapter.Fill(hostelDataSet.Students);
            hostelDataSet.AcceptChanges();
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!edit) return;
            var st = new HostelDataSet.StudentsDataTable();
            studentsTableAdapter.FillBy(
                st, Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value));
            object[] row = st.Rows[0].ItemArray;
            var edt = new EditForm(
                Convert.ToInt32(row[0]),
                row[1].ToString(),
                row[2].ToString(),
                row[3].ToString(),
                row[4].ToString(),
                row[5].ToString(),
                Convert.ToInt32(row[6]),
                Convert.ToInt32(row[7]),
                Convert.ToDateTime(row[8]));
            edt.ShowDialog();
            studentsTableAdapter.Fill(hostelDataSet.Students);
            hostelDataSet.AcceptChanges(); 
              
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!edit) return;
            studentsTableAdapter.DeleteQuery(
                Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value));
            studentsTableAdapter.Fill(hostelDataSet.Students);
            hostelDataSet.AcceptChanges();
        }

        private void editFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.studentsTableAdapter.Update(this.hostelDataSet.Students);
            this.roomsTableAdapter.Update(this.hostelDataSet.Rooms);
            this.benefitsTableAdapter.Update(this.hostelDataSet.Benefits);
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.studentsTableAdapter.Update(this.hostelDataSet.Students);
            this.roomsTableAdapter.Update(this.hostelDataSet.Rooms);
            this.benefitsTableAdapter.Update(this.hostelDataSet.Benefits);

        }
    }
}
