﻿namespace Hostel
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker_ColonizeDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_Gender = new System.Windows.Forms.ComboBox();
            this.textBox_Adress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_Group = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Benefit = new System.Windows.Forms.ComboBox();
            this.benefitsBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.hostelDataSet = new Hostel.HostelDataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Passport = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox_Room = new System.Windows.Forms.ComboBox();
            this.roomsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.benefitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.benefitsTableAdapter = new Hostel.HostelDataSetTableAdapters.BenefitsTableAdapter();
            this.benefitsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.benefitsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.roomsTableAdapter = new Hostel.HostelDataSetTableAdapters.RoomsTableAdapter();
            this.studentsTableAdapter = new Hostel.HostelDataSetTableAdapters.StudentsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostelDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(156, 6);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(100, 31);
            this.textBox_name.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата заселения";
            // 
            // dateTimePicker_ColonizeDate
            // 
            this.dateTimePicker_ColonizeDate.Location = new System.Drawing.Point(156, 44);
            this.dateTimePicker_ColonizeDate.Name = "dateTimePicker_ColonizeDate";
            this.dateTimePicker_ColonizeDate.Size = new System.Drawing.Size(200, 31);
            this.dateTimePicker_ColonizeDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Пол";
            // 
            // comboBox_Gender
            // 
            this.comboBox_Gender.FormattingEnabled = true;
            this.comboBox_Gender.Items.AddRange(new object[] {
            "мужской",
            "женский"});
            this.comboBox_Gender.Location = new System.Drawing.Point(156, 91);
            this.comboBox_Gender.Name = "comboBox_Gender";
            this.comboBox_Gender.Size = new System.Drawing.Size(121, 33);
            this.comboBox_Gender.TabIndex = 5;
            // 
            // textBox_Adress
            // 
            this.textBox_Adress.Location = new System.Drawing.Point(156, 142);
            this.textBox_Adress.Name = "textBox_Adress";
            this.textBox_Adress.Size = new System.Drawing.Size(100, 31);
            this.textBox_Adress.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Адрес";
            // 
            // textBox_Group
            // 
            this.textBox_Group.Location = new System.Drawing.Point(156, 195);
            this.textBox_Group.Name = "textBox_Group";
            this.textBox_Group.Size = new System.Drawing.Size(100, 31);
            this.textBox_Group.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Группа";
            // 
            // comboBox_Benefit
            // 
            this.comboBox_Benefit.DataSource = this.benefitsBindingSource3;
            this.comboBox_Benefit.DisplayMember = "Type";
            this.comboBox_Benefit.FormattingEnabled = true;
            this.comboBox_Benefit.Location = new System.Drawing.Point(156, 250);
            this.comboBox_Benefit.Name = "comboBox_Benefit";
            this.comboBox_Benefit.Size = new System.Drawing.Size(121, 33);
            this.comboBox_Benefit.TabIndex = 11;
            this.comboBox_Benefit.ValueMember = "Id";
            // 
            // benefitsBindingSource3
            // 
            this.benefitsBindingSource3.DataMember = "Benefits";
            this.benefitsBindingSource3.DataSource = this.bindingSource1;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = this.hostelDataSet;
            this.bindingSource1.Position = 0;
            // 
            // hostelDataSet
            // 
            this.hostelDataSet.DataSetName = "HostelDataSet";
            this.hostelDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "Тип льготы";
            // 
            // textBox_Passport
            // 
            this.textBox_Passport.Location = new System.Drawing.Point(156, 302);
            this.textBox_Passport.Name = "textBox_Passport";
            this.textBox_Passport.Size = new System.Drawing.Size(100, 31);
            this.textBox_Passport.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 25);
            this.label7.TabIndex = 12;
            this.label7.Text = "Паспорт";
            // 
            // comboBox_Room
            // 
            this.comboBox_Room.DataSource = this.roomsBindingSource;
            this.comboBox_Room.DisplayMember = "Id";
            this.comboBox_Room.FormattingEnabled = true;
            this.comboBox_Room.Location = new System.Drawing.Point(156, 361);
            this.comboBox_Room.Name = "comboBox_Room";
            this.comboBox_Room.Size = new System.Drawing.Size(121, 33);
            this.comboBox_Room.TabIndex = 15;
            this.comboBox_Room.ValueMember = "Id";
            // 
            // roomsBindingSource
            // 
            this.roomsBindingSource.DataMember = "Rooms";
            this.roomsBindingSource.DataSource = this.bindingSource1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 361);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 25);
            this.label8.TabIndex = 14;
            this.label8.Text = "Комната";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(17, 426);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(119, 57);
            this.button_OK.TabIndex = 16;
            this.button_OK.Text = "ОК";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(170, 426);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(142, 57);
            this.button_cancel.TabIndex = 17;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // benefitsBindingSource
            // 
            this.benefitsBindingSource.DataMember = "Benefits";
            this.benefitsBindingSource.DataSource = this.hostelDataSet;
            // 
            // benefitsTableAdapter
            // 
            this.benefitsTableAdapter.ClearBeforeFill = true;
            // 
            // benefitsBindingSource1
            // 
            this.benefitsBindingSource1.DataMember = "Benefits";
            this.benefitsBindingSource1.DataSource = this.bindingSource1;
            // 
            // benefitsBindingSource2
            // 
            this.benefitsBindingSource2.DataMember = "Benefits";
            this.benefitsBindingSource2.DataSource = this.bindingSource1;
            // 
            // roomsTableAdapter
            // 
            this.roomsTableAdapter.ClearBeforeFill = true;
            // 
            // studentsTableAdapter
            // 
            this.studentsTableAdapter.ClearBeforeFill = true;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 537);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.comboBox_Room);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_Passport);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox_Benefit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_Group);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_Adress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox_Gender);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker_ColonizeDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.label1);
            this.Name = "EditForm";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.EditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostelDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benefitsBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker_ColonizeDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_Gender;
        private System.Windows.Forms.TextBox textBox_Adress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_Group;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Benefit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Passport;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox_Room;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.BindingSource bindingSource1;
        private HostelDataSet hostelDataSet;
        private System.Windows.Forms.BindingSource benefitsBindingSource;
        private HostelDataSetTableAdapters.BenefitsTableAdapter benefitsTableAdapter;
        private System.Windows.Forms.BindingSource benefitsBindingSource3;
        private System.Windows.Forms.BindingSource benefitsBindingSource1;
        private System.Windows.Forms.BindingSource benefitsBindingSource2;
        private System.Windows.Forms.BindingSource roomsBindingSource;
        private HostelDataSetTableAdapters.RoomsTableAdapter roomsTableAdapter;
        private HostelDataSetTableAdapters.StudentsTableAdapter studentsTableAdapter;
    }
}